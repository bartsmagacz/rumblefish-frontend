import React, { Component } from 'react';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import config from './config';

class App extends Component {
  state = {
    blockNumber: '',
    transferredAmount: '',
    transactionsNumber: '',
    intervalSelected: '1h',
  }

  setInterval = (event) => {
    this.setState({ intervalSelected: event.target.value });
  }

  transferredAmountSubmit = (event) => {
    event.preventDefault();
    axios.get(`${config.serverURL}/contract/getTransferredAmount?time=${this.state.intervalSelected}`)
      .then((res) => {
        this.setState({ transferredAmount: res.data });
      })
  }

  transactionsNumberSubmit = (event) => {
    event.preventDefault();
    axios.get(`${config.serverURL}/contract/getTransactionsNumber?time=${this.state.intervalSelected}`)
      .then((res) => {
        this.setState({ transactionsNumber: res.data });
      })
  }

  render() {
    const selectOptionsInterval = [
      '1h', '6h', '1d'
    ]

    return (
      <div className="App">
        <nav className="navbar navbar-light bg-light mb-5">
          <span className="navbar-brand mb-0 h1">Current Block: { this.state.blockNumber }</span>
        </nav>
        <div className="container">
          <div className="row">
            <div className="h3">VeChain</div>
          </div>
          <div className="row">
            <div className="h4">Select interval</div>
            <select value={this.state.intervalSelected} onChange={this.setInterval} className="ml-4">
              { selectOptionsInterval.map(item => <option value={item} key={item}>{item}</option>)}
            </select>
          </div>
          <div className="row transferredAmount">
            <form onSubmit={this.transferredAmountSubmit}>
              <label>
                VeChain transferred:
              </label>
              <input type="text" disabled value={this.state.transferredAmount} className="m-2"/>
              <button type="submit" className="btn btn-success m-3">Get</button>
            </form>
          </div>
          <div className="row transactionsNumber">
            <form onSubmit={this.transactionsNumberSubmit}>
              <label>
                VeChain transactions:
              </label>
              <input type="text" disabled value={this.state.transactionsNumber} className="m-2"/>
              <button type="submit" className="btn btn-success m-3">Get</button>
            </form>
          </div>
        </div>
      </div>
    );
  }

  componentDidMount() {
    axios.get(`${config.serverURL}/web3/getBlockNumber`)
      .then((res) => {
        this.setState({ blockNumber: res.data });
      })
  }
}

export default App;
